# 1

## Setup environment

`pip install -r requirements.txt`

## Setup dev environment

`pip install -r requirements-dev.txt`

# 2

## Run Aiohttp server

`python -m app`

## Run Postgres with docker (local)

`docker container run -it -e POSTGRES_PASSWORD=your_password -p 5432:5432 postgres:12.2-alpine`

# Endpoints

- http://0.0.0.0:8080/  [redirect to the http://0.0.0.0:8080/redirected]
- http://0.0.0.0:8080/api/v1/error [error message (templated with jinja2)]
- http://0.0.0.0:8080/redirected [view that support multiple HTTP methods]

# Plans: 

- add prometheus client, jaeger spans
- more basic html with forms, etc
- make docker, docker-compose.yml
- create gitlab-ci.yml
- more communication with postgres
- feature: switch between postgress and sqllight (adapter~?)