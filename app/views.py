import aiohttp_jinja2
from aiohttp import web


@aiohttp_jinja2.template("index.html")
async def index(request):
    # redirection
    location = request.app.router["redir-here"].url_for()
    raise web.HTTPFound(location=location)


@aiohttp_jinja2.template("error.html")
async def error(request):
    # to catch error_msg from request
    # error = request.match_info["error_msg"]
    static_sample_error_msg = "Some Sample Error"
    return {"error_msg": str(static_sample_error_msg)}


@aiohttp_jinja2.template("redir-here.html")
async def redirect(request):
    return {"redirection_msg": "some msg"}
