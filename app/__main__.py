import logging

import aiohttp_jinja2
import jinja2
from aiohttp import web

# from prometheus_client import start_http_server

from app.config import (
    BASE_DIR,
    close_pg,
    init_pg,
    load_config_or_die,
    create_db_table_with_sql,
)

from .routes import setup_routes

logger = logging.getLogger(__name__)


async def create_app(config: dict):
    """Create Application."""

    # start_http_server(8000)
    app = web.Application()
    app["config"] = config

    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader(str(BASE_DIR / "app" / "templates")),
    )
    setup_routes(app)
    app.on_startup.append(init_pg)
    app.on_startup.append(create_db_table_with_sql)
    app.on_cleanup.append(close_pg)

    return app


if __name__ == "__main__":
    logger.info("Welcome to Aiohttp-Server-Base!")
    app = create_app(config=load_config_or_die("config.yml"))
    web.run_app(app, port=8080)
