from app.config import BASE_DIR

from .views import (
    error,
    index,
    redirect,
)


def setup_routes(app):
    app.router.add_get("/", index)
    app.router.add_get("/api/v1/error", error)
    app.router.add_view("/redirected", redirect, name="redir-here")

    # for multiple HTTP methods (ex: get and post in one)
    # app.router.add_view("/list", template_list, name="templates-list")
    # app.router.add_view(
    #     "/form/{template_name}", template_form, name="template_form"
    # )

    app.router.add_static(
        "/static/", path=BASE_DIR / "app" / "static", name="static"
    )
